import { showModal } from './modal'
import App from '../../../javascript/app'

export function showWinnerModal(fighter) {
  const title = ` ${fighter.name} wins `;
  const bodyElement = "it was a good fight";
  const onClose = restart;
  showModal({ title, bodyElement, onClose });
}

function restart(){
  let arena = document.querySelector(".arena___root");
  arena.remove();
  new App();
}
