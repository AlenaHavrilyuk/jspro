import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise(resolve => {
  console.log(`${firstFighter.name} vs ${secondFighter.name}`);

  const left = Object.assign({}, firstFighter);
  left.position = "left";
  const right = Object.assign({}, secondFighter);
  right.position = "right";
  left.startHP = left.health;
  right.startHP = right.health;
  left.holdsBlock = false;
  right.holdsBlock = false;
  let leftCriticalHitCombinationAllowed = true;
  let rightCriticalHitCombinationAllowed = true;
  let keysPressed = {};

    document.addEventListener('keydown', function(event){
      switch(event.code) {
        case controls.PlayerOneAttack:         
          if(!left.holdsBlock){
          normalHit(left, right);
          }
          if (checkIfEnemyDefeated(right)) {
            resolve(left);
          }
          break;
        case controls.PlayerTwoAttack:
          if(!right.holdsBlock){          
          normalHit(right, left);
          }
          if (checkIfEnemyDefeated(left)) {
            resolve(right);
          }
          break;
        case controls.PlayerOneBlock:           
          left.holdsBlock = true;
          console.log(`${left.name} holds block`);
          break;
        case controls.PlayerTwoBlock: 
          right.holdsBlock = true;
          console.log(`${right.name} holds block`);
          break;
        case controls.PlayerOneCriticalHitCombination[0]:
        case controls.PlayerOneCriticalHitCombination[1]:
        case controls.PlayerOneCriticalHitCombination[2]:
              keysPressed[event.code] = true;
              if (keysPressed[controls.PlayerOneCriticalHitCombination[0]]
                  && keysPressed[controls.PlayerOneCriticalHitCombination[1]]
                  && keysPressed[controls.PlayerOneCriticalHitCombination[2]]) {
                
                  console.log("crit left");
                  if (leftCriticalHitCombinationAllowed) {
                    leftCriticalHitCombinationAllowed = false;
                    criticalHit(left, right);
                    setTimeout( function() { leftCriticalHitCombinationAllowed = true; } , 10000);
                  }
                  if (checkIfEnemyDefeated(right)) {
                      resolve(left);
                    }
              }        
              break;
        case controls.PlayerTwoCriticalHitCombination[0]:
        case controls.PlayerTwoCriticalHitCombination[1]:
        case controls.PlayerTwoCriticalHitCombination[2]:
                  keysPressed[event.code] = true;
                  if (keysPressed[controls.PlayerTwoCriticalHitCombination[0]]
                      && keysPressed[controls.PlayerTwoCriticalHitCombination[1]]
                      && keysPressed[controls.PlayerTwoCriticalHitCombination[2]]) {
                    
                      console.log("crit right");
                      if (rightCriticalHitCombinationAllowed) {
                        rightCriticalHitCombinationAllowed = false;
                        criticalHit(right, left);
                        
                        setTimeout( function() { rightCriticalHitCombinationAllowed = true; } , 10000);
                      }
                      if (checkIfEnemyDefeated(left)) {
                          resolve(right);
                      }
                  }        
          break;
    }});
  
    document.addEventListener('keyup', function(event){
      switch(event.code) {
      case controls.PlayerOneBlock:           
        left.holdsBlock = false;
        console.log(`${left.name} doesn't hold block`);
        break;
      case controls.PlayerTwoBlock:           
        right.holdsBlock = false;
        console.log(`${right.name} doesn't hold block`);
        break;
              
    }
    delete keysPressed[event.code];
  });
      if(left.health === 0){
        resolve(right);
      }
      if(right.health === 0){
        resolve(left);
      }  
   })
} 

export function checkIfEnemyDefeated (enemy) {
  console.log((!enemy.health) ? true : false);
  return (!enemy.health) ? true : false;
}

export function normalHit(attacker, defender){
  console.log(`${attacker.name} attack ${defender.name}`);
  const damage = getDamage(attacker, defender);
  indicateHealth(defender, damage);
}

export function criticalHit(attacker, defender){
  console.log(`${attacker.name} makes criticai hit to ${defender.name}`);
  const damage = getCriticalDamage(attacker, defender);
  indicateHealth(defender, damage);
}

function indicateHealth(defender, damage){
  defender.health = defender.health > damage ? defender.health - damage : 0;
  const width = Math.round(defender.health / defender.startHP * 100) + "%";
  document.getElementById( `${defender.position}-fighter-indicator` ).style.width = width;
}

export function getDamage(attacker, defender) {
  // return damage
  const blockPower = defender.holdsBlock ? getBlockPower(defender) : 0;
  const hitPower = getHitPower(attacker);
  if ( blockPower > hitPower ) {
    return 0;
  }
  return hitPower - blockPower;
}

export function getCriticalDamage(attacker) {
  return attacker.attack * 2;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = getRandomInclusive(1,2);
  const power = fighter.attack * criticalHitChance;
  console.log(`HitPower = ${power}`);
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = getRandomInclusive(1,2);
  const power = fighter.defense * dodgeChance;
  console.log(`BlockPower = ${power}`);
  return power;
}

function getRandomInclusive(min, max) {
    return Math.random() * (max - min + 1) + min;
}
