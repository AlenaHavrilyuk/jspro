import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  if(!fighter){
    const chooseFighterElem = createElement({
      tagName: 'div',
      className: 'choose-fighter',
    });
    chooseFighterElem.innerText = "choose your fighter"
    chooseFighterElem.style.backgroundColor = "#ffdfa7";
    return chooseFighterElem;
  }
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const {  name, attack, health, defense } = fighter;

  const fighterInfo = createElement({
    tagName: 'div',
    className: 'fighter-info',
  }
  )

  const attackElement = createElement({
    tagName: 'div',
    className: 'attack',
  });
  attackElement.innerText = `attack = ${attack}`;

  const fighterNameElement = createElement({
    tagName: 'div',
    className: 'name',
  });
  fighterNameElement.innerText = `${name}`;

  const healthElement = createElement({
    tagName: 'div',
    className: 'health',
  });
  healthElement.innerText = `health = ${health}`;

  const defenseElement = createElement({
    tagName: 'div',
    className: 'defense',
  });
  defenseElement.innerText = `defense = ${defense}`;

  const imageElement = createFighterImage(fighter);
  imageElement.style.width="100px";

  fighterInfo.append(fighterNameElement, attackElement, healthElement, defenseElement, imageElement);
  fighterInfo.style.backgroundColor = "#ffdfa7";
  fighterElement.append(fighterInfo);
  
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
